#---------------------#
# VARIABLES AND PATHS #
#---------------------#

# Variables

set -g fish_greeting ""
set -g theme_display_user "yes"
set -gx TERMINAL "termite"
set -gx TERM "xterm-256color"
set -gx SHELL "fish"
set -gx editor "nvim"
set -gx EDITOR "nvim"
set -gx Editor "nvim"
set -gx display_size 1600 900


# Paths
set -gx docfolder ~/syncthing/2015_HS/
